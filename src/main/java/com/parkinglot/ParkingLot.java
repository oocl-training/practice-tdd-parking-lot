package com.parkinglot;

import com.parkinglot.exception.NoPositionException;
import com.parkinglot.exception.UnrecognizedTicketException;

import java.util.HashMap;
import java.util.Map;

public class ParkingLot {


    private final int capacity;

    private final Map<Ticket, Car> parkingRecord;

    public ParkingLot(int capacity) {
        this.capacity = capacity;
        parkingRecord = new HashMap<>();
    }

    public boolean isNotFull() {
        return this.parkingRecord.size() < capacity;
    }

    public boolean isValidTicket(Ticket ticket) {
        if (ticket == null)
            return false;
        else
            return parkingRecord.containsKey(ticket);
    }

    public int getNumOfEmptyPosition() {
        return capacity - parkingRecord.size();
    }

    public double getRateOfEmptyPosition() {
        return getNumOfEmptyPosition() / (double) capacity;
    }

    public Ticket park(Car car) {
        if (parkingRecord.size() < 10) {
            Ticket ticket = new Ticket();
            parkingRecord.put(ticket, car);
            return ticket;
        } else {
            throw new NoPositionException("No available position.");
        }
    }

    public Car fetch(Ticket ticket) {
        if (ticket == null)
            throw new UnrecognizedTicketException("Unrecognized parking ticket.");
        if (!parkingRecord.containsKey(ticket))
            throw new UnrecognizedTicketException("Unrecognized parking ticket.");
        return parkingRecord.remove(ticket);
    }
}
