package com.parkinglot;

import com.parkinglot.exception.ParkingBoyFailedException;

import java.util.LinkedList;
import java.util.List;

public class ParkingLotServiceManager extends StandardParkingBoy {
    private final List<ParkingBoy> managementList;

    public ParkingLotServiceManager(ParkingLot... parkingLots) {
        super(parkingLots);
        managementList = new LinkedList<>();
    }

    public void addParkingBoysToManagementList(ParkingBoy... parkingBoys) {
        managementList.addAll(List.of(parkingBoys));
    }

    public Ticket specifyParkingBoyToPark(ParkingBoy parkingBoy, Car car) {
        if (managementList.contains(parkingBoy)) {
            if (parkingBoy.canPark()) {
                return parkingBoy.park(car);
            } else {
                throw new ParkingBoyFailedException("parking lots are full");
            }
        } else {
            throw new ParkingBoyFailedException("Not my managed parking boy");
        }
    }

    public Car specifyParkingBoyToFetch(ParkingBoy parkingBoy, Ticket ticket) {
        if (managementList.contains(parkingBoy)) {
            if (parkingBoy.canFetch(ticket)) {
                return parkingBoy.fetch(ticket);
            } else {
                throw new ParkingBoyFailedException("parking boy cannot fetch");
            }
        } else {
            throw new ParkingBoyFailedException("Not my managed parking boy");
        }
    }

}
