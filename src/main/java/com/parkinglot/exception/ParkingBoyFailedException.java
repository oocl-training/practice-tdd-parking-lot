package com.parkinglot.exception;

public class ParkingBoyFailedException extends RuntimeException {
    public ParkingBoyFailedException(String message) {
        super(message);
    }
}
