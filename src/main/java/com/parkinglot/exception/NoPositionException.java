package com.parkinglot.exception;

public class NoPositionException extends RuntimeException {
    public NoPositionException(String noPosition) {
        super(noPosition);
    }
}
