package com.parkinglot;


import java.util.List;

public abstract class ParkingBoy {
    protected final List<ParkingLot> parkingLots;

    protected ParkingBoy(List<ParkingLot> parkingLots) {
        this.parkingLots = parkingLots;
    }

    // variable in interface is public static final
    protected abstract Ticket park(Car car);

    protected abstract Car fetch(Ticket ticket);

    public boolean canPark() {
        return parkingLots.stream()
                          .map(ParkingLot::isNotFull)
                          .filter(Boolean::booleanValue)
                          .findFirst()
                          .orElse(false);
    }

    public boolean canFetch(Ticket ticket) {
        return parkingLots.stream()
                          .map((parkingLot -> parkingLot.isValidTicket(ticket)))
                          .filter(Boolean::booleanValue)
                          .findFirst()
                          .orElse(false);
    }
}
