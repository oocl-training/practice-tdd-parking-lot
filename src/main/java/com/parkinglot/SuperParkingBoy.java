package com.parkinglot;

import com.parkinglot.exception.NoPositionException;
import com.parkinglot.exception.UnrecognizedTicketException;

import java.util.Comparator;
import java.util.LinkedList;
import java.util.List;

public class SuperParkingBoy extends ParkingBoy {


    public SuperParkingBoy(ParkingLot... parkingLots) {
        super(new LinkedList<>(List.of(parkingLots)));
    }

    public Ticket park(Car car) {
        return parkingLots
                .stream()
                .filter(ParkingLot::isNotFull)
                .max(Comparator.comparing(ParkingLot::getRateOfEmptyPosition))
                .orElseThrow(() -> new NoPositionException("No available position."))
                .park(car);
    }

    public Car fetch(Ticket ticket) {
        return parkingLots
                .stream()
                .filter((parkingLot -> parkingLot.isValidTicket(ticket)))
                .findFirst()
                .orElseThrow(() -> new UnrecognizedTicketException("Unrecognized parking ticket."))
                .fetch(ticket);

    }
}
