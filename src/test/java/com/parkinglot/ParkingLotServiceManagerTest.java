package com.parkinglot;

import com.parkinglot.exception.ParkingBoyFailedException;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;


public class ParkingLotServiceManagerTest {
    @Test
    void should_the_car_fetched_is_original_when_specify_parking_boy_to_park_and_fetch_given_a_car() {
        // given
        Car carParked = new Car();
        ParkingLot parkingLot = new ParkingLot(10);
        ParkingBoy standardParkingBoy = new StandardParkingBoy(parkingLot);
        ParkingLotServiceManager parkingLotServiceManager = new ParkingLotServiceManager();
        parkingLotServiceManager.addParkingBoysToManagementList(standardParkingBoy);

        // when
        Ticket ticket = parkingLotServiceManager.specifyParkingBoyToPark(standardParkingBoy, carParked);
        Car carFetched = parkingLotServiceManager.specifyParkingBoyToFetch(standardParkingBoy, ticket);

        // then
        assertEquals(carParked, carFetched);
    }

    @Test
    void should_throw_exception_when_parking_cannot_do_when_specify_parking_boy_to_park_and_fetch_given_a_car() {
        // given
        Car car = new Car();
        ParkingBoy parkingBoy1 = new StandardParkingBoy(new ParkingLot(0));
        ParkingBoy parkingBoy2 = new SmartParkingBoy(new ParkingLot(10));
        ParkingBoy parkingBoy3 = new SuperParkingBoy();
        ParkingLotServiceManager parkingLotServiceManager = new ParkingLotServiceManager();
        parkingLotServiceManager.addParkingBoysToManagementList(parkingBoy1, parkingBoy2);

        // when


        // then
        Exception noEmptyException = assertThrows(ParkingBoyFailedException.class,
                                                  () -> parkingLotServiceManager.specifyParkingBoyToPark(
                                                          parkingBoy1,
                                                          car));
        assertEquals("parking lots are full", noEmptyException.getMessage());
        Exception cannotFetchException = assertThrows(ParkingBoyFailedException.class,
                                                      () -> parkingLotServiceManager.specifyParkingBoyToFetch(
                                                              parkingBoy2,
                                                              new Ticket()));
        assertEquals("parking boy cannot fetch", cannotFetchException.getMessage());
        Exception notMyManagingParkingBoy = assertThrows(ParkingBoyFailedException.class,
                                                         () -> parkingLotServiceManager.specifyParkingBoyToPark(
                                                                 parkingBoy3,
                                                                 car));
        assertEquals("Not my managed parking boy", notMyManagingParkingBoy.getMessage());
    }
}
