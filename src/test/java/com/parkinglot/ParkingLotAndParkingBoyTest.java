package com.parkinglot;

import com.parkinglot.exception.UnrecognizedTicketException;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class ParkingLotAndParkingBoyTest {
    @Test
    void should_return_a_ticket_when_park_given_a_car() {
        // given
        Car car = new Car();
        ParkingLot parkingLot = new ParkingLot(10);
        StandardParkingBoy standardParkingBoy = new StandardParkingBoy(parkingLot);
        // when
        Ticket ticket = standardParkingBoy.park(car);
        // then
        assertNotNull(ticket);
    }

    @Test
    void should_return_two_different_tickets_when_park_given_two_cars() {
        // given
        Car car1 = new Car();
        Car car2 = new Car();
        ParkingLot parkingLot = new ParkingLot(10);
        StandardParkingBoy standardParkingBoy = new StandardParkingBoy(parkingLot);
        // when
        Ticket ticket1 = standardParkingBoy.park(car1);
        Ticket ticket2 = standardParkingBoy.park(car2);
        // then
        assertNotEquals(ticket1, ticket2);
    }

    @Test
    void should_throw_five_times_exceptions_when_park_given_15_cars() {
        // given
        ParkingLot parkingLot = new ParkingLot(10);
        StandardParkingBoy standardParkingBoy = new StandardParkingBoy(parkingLot);
        // then
        int timesOfException = 0;

        for (int i = 1; i <= 15; i++) {
            Car car = new Car();
            try {
                standardParkingBoy.park(car);
            } catch (Exception e) {
                assertEquals("No available position.", e.getMessage());
                timesOfException++;
            }
        }
        assertEquals(5, timesOfException);
    }

    @Test
    void should_return_a_car_when_fetch_given_a_ticket() {
        // given
        ParkingLot parkingLot = new ParkingLot(10);
        StandardParkingBoy standardParkingBoy = new StandardParkingBoy(parkingLot);
        Car carParked = new Car();
        Ticket ticket = standardParkingBoy.park(carParked);
        // when
        Car carFetched = standardParkingBoy.fetch(ticket);
        // then
        assertEquals(carParked, carFetched);
    }

    @Test
    void should_return_null_car_when_fetch_given_a_used_ticket() {
        // given
        ParkingLot parkingLot = new ParkingLot(10);
        StandardParkingBoy standardParkingBoy = new StandardParkingBoy(parkingLot);
        Car carParked = new Car();
        Ticket ticket = standardParkingBoy.park(carParked);
        // when
        standardParkingBoy.fetch(ticket);
        // then
        Exception exception = assertThrows(UnrecognizedTicketException.class,
                                           () -> standardParkingBoy.fetch(ticket));
        assertEquals("Unrecognized parking ticket.", exception.getMessage());
    }

    @Test
    void should_return_null_car_when_fetch_given_a_wrong_ticket() {
        // given
        ParkingLot parkingLot = new ParkingLot(10);
        StandardParkingBoy standardParkingBoy = new StandardParkingBoy(parkingLot);
        Ticket ticket = new Ticket();
        // when

        // then
        Exception exception = assertThrows(UnrecognizedTicketException.class,
                                           () -> standardParkingBoy.fetch(ticket));
        assertEquals("Unrecognized parking ticket.", exception.getMessage());
    }

    @Test
    void should_return_null_car_when_fetch_given_a_null_ticket() {
        // given
        ParkingLot parkingLot = new ParkingLot(10);
        StandardParkingBoy standardParkingBoy = new StandardParkingBoy(parkingLot);
        // when

        // then
        Exception exception = assertThrows(UnrecognizedTicketException.class,
                                           () -> standardParkingBoy.fetch(null));
        assertEquals("Unrecognized parking ticket.", exception.getMessage());
    }

    @Test
    void should_return_different_cars_when_fetch_given_two_different_tickets() {
        // given
        ParkingLot parkingLot = new ParkingLot(10);
        StandardParkingBoy standardParkingBoy = new StandardParkingBoy(parkingLot);
        Car car1Parked = new Car();
        Car car2Parked = new Car();
        Ticket ticket1 = standardParkingBoy.park(car1Parked);
        Ticket ticket2 = standardParkingBoy.park(car2Parked);
        // when
        Car car1Fetched = standardParkingBoy.fetch(ticket1);
        Car car2Fetched = standardParkingBoy.fetch(ticket2);
        // then
        assertEquals(car1Parked, car1Fetched);
        assertEquals(car2Parked, car2Fetched);
        assertNotEquals(car1Fetched, car2Fetched);
    }

    @Test
    void should_return_20_tickets_when_park_given_20_cars() {
        // given
        ParkingLot parkingLot1 = new ParkingLot(10);
        ParkingLot parkingLot2 = new ParkingLot(10);
        StandardParkingBoy standardParkingBoy = new StandardParkingBoy(parkingLot1, parkingLot2);

        // when
        int timesOfParking = 0;
        for (int i = 1; i <= 20; i++) {
            Car car = new Car();
            standardParkingBoy.park(car);
            timesOfParking++;
        }

        assertEquals(20, timesOfParking);
    }

    @Test
    void should_park_a_car_in_the_second_parkingLot_when_park_given_11_cars() {
        // given
        ParkingLot parkingLot1 = new ParkingLot(1);
        parkingLot1.park(new Car());
        ParkingLot parkingLot2 = new ParkingLot(10);
        StandardParkingBoy standardParkingBoy = new StandardParkingBoy(parkingLot1, parkingLot2);

        // when
        Car carParked = new Car();
        Ticket ticket = standardParkingBoy.park(carParked);
        Car carFetched = parkingLot2.fetch(ticket);

        // then
        assertEquals(carParked, carFetched);
    }


    @Test
    void should_park_a_car_in_second_parking_lot_by_smart_parking_boy_when_park_park_given_2_cars() {
        // given
        ParkingLot parkingLot1 = new ParkingLot(10);
        parkingLot1.park(new Car());
        ParkingLot parkingLot2 = new ParkingLot(10);
        SmartParkingBoy smartParkingBoy = new SmartParkingBoy(parkingLot1, parkingLot2);

        // when
        Car carParked = new Car();
        Ticket ticket = smartParkingBoy.park(carParked);
        Car carFetched = parkingLot2.fetch(ticket);

        // then
        assertEquals(carParked, carFetched);
    }

    @Test
    void should_park_a_car_in_first_parking_lot_by_super_parking_boy_when_park_park_given_2_cars() {
        // given
        ParkingLot parkingLot1 = new ParkingLot(100);
        parkingLot1.park(new Car());
        ParkingLot parkingLot2 = new ParkingLot(10);
        parkingLot2.park(new Car());
        SuperParkingBoy superParkingBoy = new SuperParkingBoy(parkingLot1, parkingLot2);

        // when
        Car carParked = new Car();
        Ticket ticket = superParkingBoy.park(carParked);
        Car carFetched = parkingLot1.fetch(ticket);

        // then
        assertEquals(carParked, carFetched);
    }


}
