## ORID

### O (Objective)

What did we learn today? What activities did you do? What scenes have impressed you?

```
1. TDD with OOP
2. Command Pattern
3. Code review impress me
```

### R (Reflective)

Please use one word to express your feelings about today's class.

```
full
```

### I (Interpretive)

What do you think about this? What was the most meaningful aspect of this activity?

```
1. throw exception when created.
2. throwing exception is a process that consumes performance.
3. Don't add new features for testing.
4. Don't modify other object's member variable directly.
```

### D (Decisional)

Where do you most want to apply what you have learned today? What changes will you make?

```
Use TDD to guide develop step by step. I will try to test before development.
```